import cv2
import os
import numpy as np
import skimage.feature._hog as hog
from keras import Model
from keras.applications import VGG19
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from skimage import io


class FeatureExtractor:
    SCIKIT_HOG = 1
    OPENCV_HOG = 2
    VGG_19 = 3
    AUTOENCODER = 4

    def extractFeatures_scikitHOG(self, imagePath):
        pic = io.imread(imagePath)
        b, g, r = cv2.split(pic)
        orientations = 9
        pixels_per_cell = (1, 1)
        cells_per_block = (8, 8)
        visualize = False
        transform_sqrt = True
        fd_blue = hog.hog(b, orientations=orientations, pixels_per_cell=pixels_per_cell,
                          cells_per_block=cells_per_block, visualise=visualize, transform_sqrt=transform_sqrt)
        fd_green = hog.hog(g, orientations=orientations, pixels_per_cell=pixels_per_cell,
                           cells_per_block=cells_per_block, visualise=visualize, transform_sqrt=transform_sqrt)
        fd_red = hog.hog(r, orientations=orientations, pixels_per_cell=pixels_per_cell,
                         cells_per_block=cells_per_block, visualise=visualize, transform_sqrt=transform_sqrt)
        max_red_green = np.maximum(fd_red, fd_green)
        max_feature_array = np.maximum(max_red_green, fd_blue)
        return max_feature_array

    def extractFeatures_openCVHOG(self, imagePath):
        winSize = (224, 224)
        blockSize = (16, 16)
        blockStride = (8, 8)
        cellSize = (8, 8)
        nbins = 9
        derivAperture = 1
        winSigma = 4.
        histogramNormType = 0
        L2HysThreshold = 2.0000000000000001e-01
        gammaCorrection = 0
        nlevels = 64
        hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins, derivAperture, winSigma,
                                histogramNormType, L2HysThreshold, gammaCorrection, nlevels)
        winStride = (8, 8)
        padding = (8, 8)
        image = cv2.imread(imagePath)
        imageFeature = hog.compute(image, winStride, padding)
        return imageFeature

    def extractFeatures_VGG19(self, imagePath):
        base_model = VGG19(weights='imagenet')
        model = Model(input=base_model.input,
                      output=base_model.get_layer('block4_pool').output)
        img = image.load_img(imagePath, target_size=(224, 224))
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        imageFeature = model.predict(img).flatten()
        return imageFeature

    def extractFeaturesForDataSet(self, dataset_path, extractor):
        imageFeatures = []
        switcher = {
            1: FeatureExtractor.extractFeatures_openCVHOG,
            2: FeatureExtractor.extractFeatures_scikitHOG,
            3: FeatureExtractor.extractFeatures_VGG19
        }
        for f in os.listdir(dataset_path):
            filename_full = os.path.join(dataset_path, f)
            imageFeature = switcher[extractor](filename_full)
            imageFeatures.append(imageFeature)
        imageFeatures = np.array(imageFeatures)
        return imageFeatures
