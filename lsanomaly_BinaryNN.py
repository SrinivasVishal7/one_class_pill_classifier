import os
import numpy as np
from keras.preprocessing import image
from keras.models import Model
from keras.applications import VGG19
from keras.applications.imagenet_utils import preprocess_input
from matplotlib import pyplot as plt
from lsanomaly import LSAnomaly

path = "db"
print("\nReading images from '{}' directory...\n".format(path))
base_model = VGG19(weights='imagenet')
model = Model(input=base_model.input,
              output=base_model.get_layer('block4_pool').output)
print("\nLoading VGG19 pre-trained model...\n")
X = []
for f in os.listdir(path):
    filename_full = os.path.join(path, f)
    img = image.load_img(filename_full, target_size=(224, 224))
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    features = model.predict(img).flatten()
    X.append(features)

X = np.array(X)
lsanomaly = LSAnomaly()
lsanomaly.fit(X)
directory = 'test_1'

for filename in os.listdir(directory):
    if filename.endswith(".bmp"):
        filename_full = os.path.join(directory, filename)
        img = image.load_img(filename_full, target_size=(224, 224))
        plt.imshow(img)
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        features = model.predict(img).flatten()
        Y = np.array(features)
        Y = Y.reshape(1, -1)
        prediction = lsanomaly.predict_proba(Y)
        print("\nFeature vectors obtained for : {}\n".format(filename))
        print("---------------------------------------------------------------\n")
        print("\nTraining set indices : { Advance : 0, Not_Advance : 1}")
        print("\nPrediction for {} : {}".format(filename, prediction))
        print("\n----------------------------------------------------------------")
        plt.show()
