import os
import cv2
import keras.models as model
import numpy as np
from keras import Model
from keras.applications import VGG19
from keras.preprocessing import image as img_preprocess
from lsanomaly import LSAnomaly
from matplotlib import pyplot as plt
from keras import backend as K

from pyimagesearch.preprocessing import ExtractPreprocessor

crop_w = 224
crop_h = 224

MARGIN = 50


def preprocess_input(x, dim_ordering='default'):
    if dim_ordering == 'default':
        dim_ordering = K.image_dim_ordering()
    assert dim_ordering in {'tf', 'th'}

    if dim_ordering == 'th':
        x[:, 0, :, :] -= 103.939
        x[:, 1, :, :] -= 116.779
        x[:, 2, :, :] -= 123.68
        x = x[:, ::-1, :, :]
    else:
        x[:, :, :, 0] -= 103.939
        x[:, :, :, 1] -= 116.779
        x[:, :, :, 2] -= 123.68
        x = x[:, :, :, ::-1]
    return x


path = "db"
print("Reading images from '{}' directory...\n".format(path))
base_model = VGG19(weights='imagenet')
model = Model(input=base_model.input,
              output=base_model.get_layer('block4_pool').output)
print("Loading VGG19 pre-trained model...")

X = []
for f in os.listdir(path):
    filename_full = os.path.join(path, f)
    img = img_preprocess.load_img(filename_full, target_size=(224, 224))
    img = img_preprocess.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    features = model.predict(img).flatten()
    X.append(features)

X = np.array(X)
lsanomaly = LSAnomaly()
lsanomaly.fit(X)

print("\nSet indices : {'No_Advance_Crocin': 1, 'Advance_Crocin': 0}\n")

cam = cv2.VideoCapture(0)
if not cam.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

for skip in range(15):
    ret, image_cam = cam.read()
    if ret is False:
        print("image_cam not found")
        exit()
imgX, imgY = int(image_cam.shape[1] / 2), int(image_cam.shape[0] / 2)

croppedimage_cam = image_cam

first_gray = cv2.cvtColor(croppedimage_cam, cv2.COLOR_BGR2GRAY)
extimage_cam = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
count = 0
while True:
    ret, image_cam = cam.read()
    if ret is False:
        print("image_cam not found")
        exit()
    tempimage_cam = image_cam.copy()
    tempimage_cam[imgY - int(crop_h / 2), imgX - int(crop_w / 2): imgX + int(crop_w / 2)] = 255
    tempimage_cam[imgY + int(crop_h / 2), imgX - int(crop_w / 2): imgX + int(crop_w / 2)] = 255
    tempimage_cam[imgY - int(crop_h / 2): imgY + int(crop_h / 2), imgX + int(crop_w / 2)] = 255
    tempimage_cam[imgY - int(crop_h / 2): imgY + int(crop_h / 2), imgX - int(crop_w / 2)] = 255

    fullimage_cam = image_cam.copy()
    status, extractedimage_cam = extimage_cam.preprocess(image_cam)

    cv2.imshow('Cropped image', extractedimage_cam)
    cv2.imshow('Full image', tempimage_cam)
    k = cv2.waitKey(1)
    if chr(k & 255) is 'q':
        break
    elif chr(k & 255) is 's':
        count += 1
        filename = "test_image_{}.bmp".format(count)
        # filename = "Advance_844_24.bmp"
        cv2.imwrite(filename, extractedimage_cam)
        test_image_cam = img_preprocess.load_img(filename, target_size=(224, 224))
        plt.imshow(test_image_cam)
        test_image_cam = img_preprocess.img_to_array(test_image_cam)
        test_image_cam = np.expand_dims(test_image_cam, axis=0)
        test_image_cam = preprocess_input(test_image_cam)
        Y = model.predict(test_image_cam).flatten()
        Y = np.array(Y)
        Y = Y.reshape(1, -1)
        preds = lsanomaly.predict_proba(Y)
        print("\n------------------------------------------")
        print("\nPrediction :- ")
        print(preds)
        print("\n------------------------------------------")
        plt.show()
