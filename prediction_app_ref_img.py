import cv2
import numpy as np
from keras.models import load_model


from pyimagesearch.preprocessing import ExtractPreprocessor

crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture(0)
if not cam.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

model = load_model('model_approach_2.h5')
print("[INFO] loading network...")

labels = ['Advance', 'Not Advance']
print('classes : ', len(labels))
score = [None] * (len(labels))
print('labels : ', labels)

NUM_CLASSES = len(labels)

imageCount = 0

for skip in range(10):
    ret, image = cam.read()
    if ret is False:
        print("image not found")
        exit()
imgX, imgY = image.shape[1] / 2, image.shape[0] / 2

croppedImage = image

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)


def fineTunePrediction(image):
    tempImage = image.copy()

    tempImage[int(imgY) - int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2)] = 0
    status, extractedImage = extImage.preprocess(image)
    cv2.imshow("extractedImage", extractedImage)

    image = extractedImage.copy()

    if NUM_CLASSES < 20:
        num_row = int(NUM_CLASSES % 20)
    else:
        num_row = 19
    canvas = np.zeros((35 * (1 + num_row) + 5, 350 * (1 + int(NUM_CLASSES / 20)), 3), dtype="uint8")
    croppedImage = image.copy()
    image = np.expand_dims(image, axis=0)
    preds = model.predict_proba(image, batch_size=1, verbose=0)
    pred = np.argmax(preds, axis=1)
    for cl in range(len(labels)):
        val = preds[0][cl] * 100
        score[cl] = round(val, 2)
    best_score = score[int(pred)]

    for cl in range(len(labels)):
        w = int(score[cl] * 3)
        if cl == pred:
            cv2.rectangle(canvas, (5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 5),
                          (w + 5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 35), (255, 0, 0), -1)
        else:
            cv2.rectangle(canvas, (5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 5),
                          (w + 5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 35), (0, 0, 255), -1)
        cv2.putText(canvas, labels[int(cl)] + ' ' + str(score[cl]), (10 + 400 * int(cl / 20), (int(cl % 20) * 35) + 23),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)
    cv2.putText(tempImage, labels[int(pred)] + ' ' + str(best_score), (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

    cv2.imshow('croppedImage', croppedImage)
    cv2.imshow('probabilities', canvas)
    cv2.imshow('frame', tempImage)
    k = cv2.waitKey(1)
    if chr(k & 255) is 'q':
        exit()


while True:
    ret, image = cam.read()
    if ret is False:
        print("image not found")
        exit()
    fineTunePrediction(image)

f.close()
